package miniproject.serviceBackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import miniproject.serviceBackend.entity.AdminCarService;
import miniproject.serviceBackend.entity.AdminMongoTable;
import miniproject.serviceBackend.entity.CarService;
import miniproject.serviceBackend.repository.AdminMongo;
import miniproject.serviceBackend.repository.CarServiceRepo;
import miniproject.serviceBackend.service.Service;
import miniproject.serviceBackend.serviceIMPL.AdminService;
import miniproject.serviceBackend.serviceIMPL.UserService;

@CrossOrigin(origins =  "http://localhost:4200/")
@RestController
public class CarController {
	

	
	 
	@Autowired
	private CarServiceRepo carServiceRepo;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminMongo adminMono;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private Service service;
	
	@GetMapping("/user")
	public  List<CarService> getUser(){
		return service.getAllUser();
	}
   
	@PostMapping("/user")
	public CarService postUser(@RequestBody CarService carService) {
		return carServiceRepo.save(carService);
	}
	
	@PostMapping("/userlog")
	 public ResponseEntity<CarService>get(@RequestBody CarService  carService ){
		CarService cS=userService.login(carService);
		if(cS.getPassword().equals(carService.getPassword()))
			return ResponseEntity.ok(cS);
			return (ResponseEntity<CarService>) ResponseEntity.internalServerError();
		
	}

		@PostMapping("/admin")
		public ResponseEntity<AdminCarService>get(@RequestBody AdminCarService admin){
			AdminCarService adminCarService=adminService.login(admin);
			System.out.println(adminCarService);
			if(adminCarService.getPassword().equals(admin.getPassword()))
			return ResponseEntity.ok(adminCarService);
			return (ResponseEntity<AdminCarService>) ResponseEntity.internalServerError();
			
		}
	
//	----------------------------------------------------------------------------
	
		@GetMapping("/adminconfig")
		public  List<AdminMongoTable> getUser1(){
			return adminMono.findAll();
		}
		
		
		@PostMapping("/adminconfig")
		public AdminMongoTable postUser(@RequestBody AdminMongoTable adminMongoTable) {
			return adminMono.save(adminMongoTable);
		}
	
	
	
	
	
	
}
