package miniproject.serviceBackend.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;

@Data
@Document(collection="config")
public class AdminMongoTable {

	
	private int rating;
	private int companyrating;
	@Override
	public String toString() {
		return "AdminMongoTable [rating=" + rating + ", companyrating=" + companyrating + "]";
	}
	public AdminMongoTable() {
		super();
	}
	public AdminMongoTable(int rating, int companyrating) {
		super();
		this.rating = rating;
		this.companyrating = companyrating;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getCompanyrating() {
		return companyrating;
	}
	public void setCompanyrating(int companyrating) {
		this.companyrating = companyrating;
	}
	
	
	
	
}
