package miniproject.serviceBackend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="admin")
public class AdminCarService {
	
	@Id
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;

	@Override
	public String toString() {
		return "AdminCarService [username=" + username + ", password=" + password + "]";
	}

	public AdminCarService() {
		super();
	}

	public AdminCarService(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
