package miniproject.serviceBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import miniproject.serviceBackend.entity.AdminCarService;
@Repository
public interface AdminRepository extends JpaRepository<AdminCarService, String> {

	AdminCarService findByUsername(String username);

}
