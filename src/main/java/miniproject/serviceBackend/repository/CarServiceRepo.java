package miniproject.serviceBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import miniproject.serviceBackend.entity.CarService;

@Repository
public interface CarServiceRepo extends JpaRepository<CarService, Integer>{

	CarService findByUsername(String username);

	
	

	
	
	
}
