package miniproject.serviceBackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import miniproject.serviceBackend.entity.AdminMongoTable;


@Repository
public interface AdminMongo extends MongoRepository<AdminMongoTable, Integer> {

}
