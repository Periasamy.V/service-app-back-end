package miniproject.serviceBackend.serviceIMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import miniproject.serviceBackend.entity.AdminCarService;
import miniproject.serviceBackend.repository.AdminRepository;

@Service
public class AdminService {
	@Autowired
	private AdminRepository adminRepository;
	
	public AdminCarService login(AdminCarService adminCarService) {
		AdminCarService admin=adminRepository.findByUsername(adminCarService.getUsername());
		return admin;
	}

}
