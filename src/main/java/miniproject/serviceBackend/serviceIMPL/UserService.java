package miniproject.serviceBackend.serviceIMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import miniproject.serviceBackend.entity.CarService;
import miniproject.serviceBackend.repository.CarServiceRepo;

@Service
public class UserService {
	
	@Autowired
	private CarServiceRepo carServiceRepo;
	
	public CarService login(CarService carService) {
		CarService user=carServiceRepo.findByUsername(carService.getUsername());
		return user;
	}
}
