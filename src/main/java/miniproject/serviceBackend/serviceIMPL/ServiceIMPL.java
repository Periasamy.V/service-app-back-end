package miniproject.serviceBackend.serviceIMPL;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import miniproject.serviceBackend.entity.CarService;
import miniproject.serviceBackend.repository.CarServiceRepo;

@Service
public class ServiceIMPL implements miniproject.serviceBackend.service.Service {

	@Autowired
	private CarServiceRepo carServiceRepo;

	@Override
	public List<CarService> getAllUser() {
		return carServiceRepo.findAll();
	}




}
